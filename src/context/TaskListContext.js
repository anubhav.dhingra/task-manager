import React, { createContext,useState } from 'react';
import {v4 as uuid} from 'uuid';
export const TaskListContext = createContext();

const TaskListContextProvider = (props) => {
    
    const [tasks, setTasks] = useState([
        {title: 'Read a book', id: 1},
        {title: 'Write Code', id: 2},
        {title: 'Wash Bike', id: 3},
    ]);

    const [editItem, setEditItem] = useState(null);

    const addTask = (title) => {
        setTasks([...tasks, {title, id: uuid()}])
    };

    const deleteTask = (id) => {
        const currentTasks = [...tasks];
        setTasks(currentTasks.filter(task => task.id !== id));
    }

    const clearList = () => {
        setTasks([]);
    }

    const findTask = (id) => {
        const item = tasks.find(task => task.id === id);
        setEditItem(item)
    }

    const editTask = (title, id) => {
        const newTasks = tasks.map((task) => task.id === id ? {title, id}: task);
        setTasks(newTasks);
        setEditItem('');
    }

    return (
        <TaskListContext.Provider value={{tasks, clearList, addTask, deleteTask, findTask, editItem, editTask}}>
            {props.children}
        </TaskListContext.Provider>
    )
}

export default TaskListContextProvider;